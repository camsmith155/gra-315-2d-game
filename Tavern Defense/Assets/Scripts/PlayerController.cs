﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float redcd, bluecd, greencd, bulletFire;

    [SerializeField]
    Material green;
    [SerializeField]
    Material blue;
    [SerializeField]
    Material red;
    [SerializeField]
    private float speed;
    [SerializeField]
    //prefab bullet
    private GameObject Bullet;
    //current bullet when fired
    private GameObject CurBullet;
    private float joystickMovement;
    private Input buttonPress;
    private enum attackType { Block, Sword, Bow };

    [SerializeField]
    private AudioClip shot;
    [SerializeField]
    private AudioSource source;
    private void Start()
    {
    }
    private void Update()
    {
        greencd -= Time.deltaTime;
        bluecd -= Time.deltaTime;
        redcd -= Time.deltaTime;
        bulletFire -= Time.deltaTime;


        GatherInput();
        Movement(joystickMovement);
    }

    private void GatherInput()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Attack(attackType.Block);
        }
        if (Input.GetButtonDown("Fire2"))
        {
            Attack(attackType.Sword);
        }
        if (Input.GetButtonDown("Fire3"))
        {
            Attack(attackType.Bow);
        }

        if (!(Input.GetAxis("Horizontal") < 0.2f && Input.GetAxis("Horizontal") > -0.2f))
            joystickMovement = Input.GetAxis("Horizontal");
        else
            joystickMovement = 0;
        //Debug.Log(joystickMovement);

    }
    private void Movement(float movement)
    {
        //Move the player left and right
        this.transform.position += new Vector3(joystickMovement * speed, 0, 0);
        //If the player moves too far in one direction, warp them to the other side
        if (this.transform.position.x >= 6.5f || this.transform.position.x <= -6.5f)
        {
            this.transform.position = new Vector3(-this.transform.position.x, this.transform.position.y, this.transform.position.z);
        }

    }

    private void Attack(attackType aAttack)
    {
        if (bulletFire <= 2.5f)
        {
            Debug.Log("I just made a " + aAttack + " attack!");
            switch (aAttack)
            {

                case attackType.Block:
                 //   if (greencd <= 0)
                    {
                        CurBullet = Instantiate(Bullet, this.transform.position, this.transform.rotation);
                        source.PlayOneShot(shot);

                        greencd += 1f;
                        CurBullet.GetComponentInChildren<MeshRenderer>().material = green;
                        CurBullet.tag = "green";
                    }
                    //green
                    break;
                case attackType.Sword:
               //     if (redcd <= 0)
                    {
                        CurBullet = Instantiate(Bullet, this.transform.position, this.transform.rotation);
                        source.PlayOneShot(shot);

                        redcd += 1f;
                        CurBullet.GetComponentInChildren<MeshRenderer>().material = red;
                        CurBullet.tag = "red";
                    }

                    break;
                case attackType.Bow:
               //     if (bluecd <= 0)
                    {
                        CurBullet = Instantiate(Bullet, this.transform.position, this.transform.rotation);
                        source.PlayOneShot(shot);

                        bluecd += 1f;
                        CurBullet.GetComponentInChildren<MeshRenderer>().material = blue;
                        CurBullet.tag = "blue";
                    }

                    break;
                default:
                    Debug.Log("Exepected Error - Attack");
                    break;
            }
            bulletFire+=.45f;
        }
    }

}
