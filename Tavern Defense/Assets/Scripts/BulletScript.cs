﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private float time;
    PlayerController player;

    private void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        time += Time.deltaTime;
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + speed, this.transform.position.z);
        if(time >= 1.5f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" && other.GetComponent<EnemyBehavior>().color == this.tag)
        {
            if(this.tag == "red")
            {
                player.redcd--;
            }
            if (this.tag == "green")
            {
                player.greencd--;
            }
            if (this.tag == "blue")
            {
                player.bluecd--;
            }
            other.gameObject.GetComponent<ShowPopUp>().EnemyKilled();
            Debug.Log("Just hit an enemy!");
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
    }
}
