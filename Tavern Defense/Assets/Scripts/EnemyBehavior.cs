﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    GameObject[] Tavern;
    bool alive;
    [SerializeField]
    public string color;
    public float speed;
    public float attackRange;
    private float distanceToTavern;
    [SerializeField]
    private int aRan;
    [SerializeField]
    Stats aStat;

    void Awake()
    {
        aStat = GameObject.FindObjectOfType<Stats>();
        Tavern = aStat.Tavern;
    }

    // Start is called before the first frame update
    void Start()
    {
       aRan = Random.Range(0,aStat.activeTaverns);
        alive = true;
        //StartCoroutine("GentleSpawn");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        

        if (aRan > Tavern.Length)
        {
            aRan = Random.Range(0, aStat.activeTaverns);
        }

        if (Tavern[aRan].activeInHierarchy == false)
        {
            aRan = Random.Range(0, aStat.activeTaverns);
        }

        if (aRan > Tavern.Length || Tavern[aRan] == null)
        {
            //aRan = Random.Range(0, aStat.activeTaverns);
            return;
        }
        //Tavern attack logic

        transform.LookAt(Tavern[aRan].transform.position);

        distanceToTavern = Vector3.Distance(transform.position, Tavern[aRan].transform.position);

        if (distanceToTavern < attackRange)
        {
            Vector3 aDirection = (Tavern[aRan].transform.position - transform.position);
            float angle = Mathf.Atan2(aDirection.y, aDirection.x) * Mathf.Rad2Deg - 90f;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 100);

            transform.Translate(Vector2.up * Time.deltaTime * speed);
        }
    }

    //back-and-forth movement
    IEnumerator GentleSpawn()
    {
        while (alive = true)
        {
            transform.position = new Vector3(Mathf.PingPong(Time.time, 1), transform.position.y, transform.position.z);
            yield return new WaitForEndOfFrame();
        }
    }

    /*public void OnTrigerEnter(Collider other)
    {
        if (other.tag == "Weapon")
        {
            Destroy(other.gameobject);
            DestroyMe();
        }

        if(other.tag == "Tavern")
        {
            //tavern damage logic
            DestroyMe();
        }
    }

    private void DestroyMe()
    {
        alive = false;
        DestroyMe(gameobject);
    }

    private void OnDestroy()
    {
        //logic for adding score
    }*/
}
