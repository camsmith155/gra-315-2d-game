﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject Enemy;
    bool tavernUp;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Spawn");
        tavernUp = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Spawn()
    {
        while (tavernUp == false)
        {
            yield return new WaitForSeconds(1);
        }

        while (tavernUp == true)
        {
            yield return new WaitForSeconds(5);
            Instantiate(Enemy, gameObject.transform.position, gameObject.transform.rotation);
            yield return new WaitForSeconds(5);
        }

        yield return new WaitForSeconds(2);
    }
}
