﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomInfiniteSpawner : MonoBehaviour
{
    /// <summary>
    /// Created by Nathan Dalessio
    /// 
    /// This script is meant to infinitely spawn enemies with changing variables over time
    /// Variables:
    /// - What enemies can spawn
    /// - How fast the eneimies are
    /// - How frequently the enemies spawn 
    /// </summary>
    /// 


    // The prefabs for the enemies
    [SerializeField]
    List<GameObject> enemyList;
    public GameObject Slime;
    public GameObject Lizard;
    public GameObject Dragon;

    [SerializeField]
    List<GameObject> SpawnPointList;
    public GameObject SpawnPointOne;
    public GameObject SpawnPointTwo;
    public GameObject SpawnPointThree;
    public GameObject SpawnPointFour;

    private bool level1;
    private bool level2;
    private bool level3;
    private bool playing;

    private static bool startGame;

    [Tooltip("set equal to the time it should take to reach level 2")] public int level2Time;
    [Tooltip("set equal to the time it should take to reach level 3")] public int level3Time;

    [Tooltip("This is how fast in seconds the enemy waves will spawn")] public float spawnRate;
    [Tooltip("This is how many seconds to take off the spawn rate per level")] public float decreaseSpawnRate;

    private float timer;
    private int whoToSpawn = 1;
    private int howManyCanSpawn = 4;
    [SerializeField]
    List<int> temp = new List<int>();

    private void Start()
    {
        enemyList.Add(Slime); // 0
        enemyList.Add(Lizard); // 1
        enemyList.Add(Dragon); // 2

        SpawnPointList.Add(SpawnPointOne);
        SpawnPointList.Add(SpawnPointTwo);
        SpawnPointList.Add(SpawnPointThree);
        SpawnPointList.Add(SpawnPointFour);

        level1 = true;
        level2 = false;
        level3 = false;

        temp.Add(-1);
    }

    private void FixedUpdate()
    {
        startGame = Stats.startGameAll;

        if (playing)
            timer += Time.fixedDeltaTime;

        if (startGame && !playing)
        {
            playing = true;
            StartCoroutine(Spawner());
            //startGame = false;
        }

        if (level2 == false && timer < level3Time && timer > level2Time) // set level 2
        {
            level2 = true;
            level1 = false;
            level3 = false;

            whoToSpawn = 2; // spawn slimes and lizards
            howManyCanSpawn = 6;
            spawnRate -= decreaseSpawnRate;
        }
        else if (level3 == false && timer > level3Time) // set level 3
        {
            level3 = true;
            level1 = false;
            level2 = false;

            whoToSpawn = 3; // spawn slimes lizards and dragons
            howManyCanSpawn = 8;
            spawnRate -= decreaseSpawnRate;
        }
    }

    public void Died()
    {
        playing = false;
    }

    public IEnumerator Spawner()
    {
        while (playing)
        {
            yield return new WaitForSeconds(spawnRate);

            int spawnPointIndex = 0; // define spawnPointIndex
            int numEnemiesToSpawn;
            if (level1)
                numEnemiesToSpawn = Random.Range(1, howManyCanSpawn); // choose a random number of enemies to spawn
            else if (level2)
                numEnemiesToSpawn = Random.Range(3, howManyCanSpawn); 
            else
                numEnemiesToSpawn = Random.Range(4, howManyCanSpawn);


            // Spawn the desired number of enemies 
            for (int i = 0; i < numEnemiesToSpawn; i++)
            {
                int prefabIndex = Random.Range(0, whoToSpawn); // choose between what enemies are available to spawn

                // Set the spawn point
                if (numEnemiesToSpawn > 1) // if theres multiple enemies spawning this wave...
                {
                    spawnPointIndex = Random.Range(0, 4);               // choose a spawn point
                    foreach (int num in temp)
                    {
                        if (spawnPointIndex == num)                     // if the spawn point has been used already choose a different point
                        {
                            while (spawnPointIndex == num)
                            {
                                spawnPointIndex = Random.Range(0, 4);   // choose a different spawn point
                            }
                        }
                    }
                }
                else                                        // else if just one enmy is spawning this wave...
                {
                    spawnPointIndex = Random.Range(0, 4);   // choose a random spawn point for the one enemy
                }

                temp.Add(spawnPointIndex);                  // update the last used spawn point index

                yield return new WaitForSeconds(.5f); // with some delay between each spawn, spawn the enemy
                Instantiate(enemyList[prefabIndex], SpawnPointList[spawnPointIndex].gameObject.transform.position, Quaternion.identity);
            }

            if (temp.Count != 0)
                temp.Clear(); // reset the temp list
        }
    }
}
