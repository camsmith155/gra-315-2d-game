﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stats : MonoBehaviour
{
    /// <summary>
    /// Created by Nathan Dalessio
    /// 
    /// This script is meant to act as a stats holding script 
    /// for variables that are shared or affected by multiple objects
    /// including the overall health of all bases combined 
    /// 
    /// SETUP: 
    /// to make this script work set the health bar object in the editor
    /// and the death panel canvas object for death screen 
    /// 
    /// </summary>
    public int maxTaverns;
    public int activeTaverns = 12;
    [SerializeField]
    public static Stats statsInstance;
    // RandomInfiniteSpawner spawnerInstance = new RandomInfiniteSpawner();
    [SerializeField]
    private GameObject startButton;

    [SerializeField]
    Text ScoreText;
    public static int overallHealth;
    public static float Score;
    public static GameObject healthBar;

    public Text scoreText;
    [Tooltip("This is the amount that the score goes up based on time")]
    public float timerScoreAmount;

    [Tooltip("set to the healthbar image")]
    public GameObject setHealthBar; // set healthBar object equal to this 

    [Tooltip("set to whatver will display when you die")]
    public GameObject deadScreen; // set this to the death panel 

    GameObject[] allSpawns;

    public static bool startGameAll;
    private float timer;
    public GameObject[] Tavern;

    private void Start()
    {
        allSpawns = GameObject.FindGameObjectsWithTag("Spawner");
        ScoreText.text = "Score: " + Score;
        statsInstance = this;

        /*if (statsInstance == null)
        {
        }
        else
        {
            Destroy(this);
        }*/

        overallHealth = 12;
        Score = 0;
        timer = 0;

   //     healthBar = setHealthBar;
        statsInstance.deadScreen = deadScreen;
        deadScreen.SetActive(false);
    }


    private void FixedUpdate()
    {
        Tavern = GameObject.FindGameObjectsWithTag("Tavern");


        if (startGameAll)
        {
            timer += Time.fixedDeltaTime;

            Score += timerScoreAmount;
            scoreText.text = Score.ToString();
        }
        if(activeTaverns <= 0)
        {
            startGameAll = false;
            Die();
            deadScreen.SetActive(true);
        }
    }

    public void StartGame() // begin spawning 
    {
        startButton.SetActive(false);
        startGameAll = true;
    }

   /* public void OverallDamage()
    {
        overallHealth--;

        if (overallHealth <= 0)
            overallHealth = 0; // make sure health doesn't go negative 

     //   healthBar.transform.localScale = new Vector3(overallHealth / 12f, 1, 1); // set the size of the health bar 


        // death
        if (overallHealth <= 0)
        {
            statsInstance.deadScreen.SetActive(true);
            //          Die();
            startGameAll = false;
            Die();
        }

    }*/

    public void AddScore(int aScore)
    {
        if (startGameAll == true)
        {
            Score += aScore;
        }
    }

    public void Die()
    {
        for(int i=0; i<allSpawns.Length; i++)
        {
            allSpawns[i].GetComponent<RandomInfiniteSpawner>().Died();
        }
        //FindObjectsOfType<RandomInfiniteSpawner>().spawnerInstance.Died();
    }
}
