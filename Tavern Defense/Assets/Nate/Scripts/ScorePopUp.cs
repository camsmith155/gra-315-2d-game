﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorePopUp : MonoBehaviour
{
    public float speed;
    void Update()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(0, 0, 0), speed * Time.deltaTime);
        Destroy(gameObject, 3f);
    }
}
