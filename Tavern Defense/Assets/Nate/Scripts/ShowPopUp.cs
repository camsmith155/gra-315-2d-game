﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowPopUp : MonoBehaviour
{
    /// <summary>
    /// Created by Nathan Dalessio
    /// 
    /// SETUP:
    /// Put this script on the enemies
    /// call collision.gameObject.GetComponent<ShowPopUp>().EnemyKilled();
    /// on the bullet to add score from killing enemy
    /// </summary>
    Stats statsInstance;

    public GameObject ScorePopUpText;
    public Canvas canvas;
    private void Start()
    {
        statsInstance = GameObject.FindGameObjectWithTag("GameController").GetComponent<Stats>();
        canvas = GameObject.FindGameObjectWithTag("ScorePopUpCanvas").GetComponent<Canvas>();
    }

    public void EnemyKilled()
    {
        Instantiate(ScorePopUpText, transform.position, Quaternion.identity, canvas.transform); // instantiate a pop up of added score
        statsInstance.AddScore(100); // add to score 
    }
}
