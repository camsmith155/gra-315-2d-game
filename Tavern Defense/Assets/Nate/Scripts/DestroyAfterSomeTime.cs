﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterSomeTime : MonoBehaviour
{
    void Update()
    {
        transform.Translate(-Vector3.up * 1.5f * Time.deltaTime);
        Destroy(gameObject, 10f);
    }   
}
