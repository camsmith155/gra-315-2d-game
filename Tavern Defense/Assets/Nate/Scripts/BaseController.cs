﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseController : MonoBehaviour
{
    /// <summary>
    /// Created by Nathan Dalessio
    /// 
    /// This script is meant to control the base or tavern objects
    /// they will have health and different visuals for health representation 
    /// visuals and stats will be added and tweakable in the editor 
    /// 
    /// SETUP:
    /// To make this script work set the 4 sprite images for the different health levels 
    /// and set the layer number that is associated with the enemies 
    /// 
    /// </summary>

        [SerializeField]
    Stats statsInstance;

    // Variables

    [Tooltip("Set to the layer number that the enemy layer is associated with")] public int enemyLayerNum = 0;

    [Tooltip("Destroyed image for no health left")] public Sprite health_0_Image;
    [Tooltip("Very damaged image for 1 health left")] public Sprite health_1_Image;
    [Tooltip("Slightly damaged image for 2 health left")] public Sprite health_2_Image;
    [Tooltip("Normal image for 3 or full health left")] public Sprite health_3_Image;

    Sprite BaseImage; // the current image of this base
    [SerializeField]
    int health = 3; // the current health of this base

    private void Start()
    {
        statsInstance = GameObject.FindGameObjectWithTag("GameController").GetComponent<Stats>();
    }

    public void TakeDamage()
    {
        --health; // deduct one hit point 

        // set corresponding image based on updated health int
        if (health <= 0) { BaseImage = health_0_Image; }
        else if (health == 3)
            BaseImage = health_3_Image;
        else if (health == 2)
            BaseImage = health_2_Image;
        else if (health == 1)
            BaseImage = health_1_Image;

        if (health >= 0) // only update health and image if it hasn't already hit zero 
        {
            --statsInstance.activeTaverns;
            //statsInstance.OverallDamage(); // update the overall health/damage taken
            GetComponent<SpriteRenderer>().sprite = BaseImage; // update the image
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            Destroy(other.gameObject);
            TakeDamage();
            //collision.gameObject.GetComponent<ShowPopUp>().EnemyKilled(); // FOR TESTING - COMMENT OUT
            Destroy(other.gameObject);
        }
    }
}
