﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    /// <summary>
    /// Created by Nathan Dalessio
    /// 
    /// this script is for the use of buttons
    /// put the object associated with this script on a button event
    /// and choose the desired function on click 
    /// </summary>

    string sceneName;
    //RandomInfiniteSpawner SpawnInstance = new RandomInfiniteSpawner();
    public Stats statsInstance;
    public GameObject StartPanel;
    public GameObject DeadPanel;

    private void Start()
    {
        
        DeadPanel.SetActive(false);
        StartPanel.SetActive(true);
       // statsInstance = <Stats>();
        sceneName = SceneManager.GetActiveScene().name;
    }

    public void Update()
    {
        if (Stats.startGameAll != true && Stats.Score <= 0)
        {
            if(Input.GetButtonDown("Fire1"))
            {
                Play();
            }
        }
        else if(Stats.startGameAll != true && Stats.Score > 0)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Restart();
            }
        }
    }

    public void Play()
    {
        statsInstance.StartGame();
        StartPanel.SetActive(false); // turn off the start screen 
    }


  /*  public void Die()
    {
        SpawnInstance.Died();
    }*/

    public void Restart()
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
}
